//
//  AppDelegate.swift
//  Commited To Music
//
//  Created by Simon Knapp on 3/28/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    lazy var authorizationManager: AuthManager = {
        return AuthManager(appleMusicManager: self.appleMusicManager)
    }()
    
    var musicPlayerManager = MusicPlayerManager()
    var appleMusicManager = MusicManager()
    
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        guard let homeVC = topViewControllerAtTabBarIndex(0) as? HomeVC else {
            fatalError("Unable to find expected \(HomeVC.self) in at TabBar Index 0")
        }
        
        guard let playerVC = topViewControllerAtTabBarIndex(1) as? PlaybackVC else {
            fatalError("Unable to find expected \(PlaybackVC.self) in at TabBar Index 2")
        }
        
        guard let searchCollectionCellVC = topViewControllerAtTabBarIndex(2) as? SearchCollectionCellVC else {
            fatalError("unable to find expected \(SearchCollectionCellVC.self) at tab index 3")
        }
        
        application.beginReceivingRemoteControlEvents()
        
        remoteCommandManager = RemoteCommandManager(musicPlayerManager: musicPlayerManager)
        remoteCommandManager.disablePlaybackControls()
        
        homeVC.authorizationManager = authorizationManager
        
        playerVC.musicPlayerManager = musicPlayerManager
        
        searchCollectionCellVC.authorizationManager = authorizationManager
        searchCollectionCellVC.musicPlayerManager = musicPlayerManager
        
        createAudioSession()
        
        return true
    }
    
    func createAudioSession() {
     
        let audioSession = AVAudioSession.sharedInstance()
        
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayback, mode: AVAudioSessionModeDefault)
        }
        catch {
            print("An error occured setting the audio session category: \(error)")
        }
        
        // Set the AVAudioSession as active.  This is required so that your application becomes the "Now Playing" app.
        do {
            try audioSession.setActive(true, with:[])
        }
        catch {
            print("An Error occured activating the audio session: \(error)")
        }
    }

    func topViewControllerAtTabBarIndex(_ index: Int) -> UIViewController? {
        guard let tabBarController = window?.rootViewController as? UITabBarController,
            let navigationController = tabBarController.viewControllers?[index] as? UINavigationController else {
                fatalError("Unable to find expected View Controller in Main.storyboard.")
        }
        
        return navigationController.topViewController
    }

}

