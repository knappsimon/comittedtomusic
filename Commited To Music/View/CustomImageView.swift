//
//  CustomImageView.swift
//  Commited To Music
//
//  Created by Simon Knapp on 4/9/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import UIKit

@IBDesignable
class CustomImageView: UIImageView {

    
    @IBInspectable var bgColor: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0) {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable var cornerRad: CGFloat = 10 {
        didSet {
            setupView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    override func prepareForInterfaceBuilder() {
        setupView()
    }
    
    func setupView() {
        layer.cornerRadius = cornerRad
        layer.backgroundColor = bgColor.cgColor
        clipsToBounds = true
    }
    
}
