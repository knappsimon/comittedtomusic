//
//  CustomPlaybackBtnView.swift
//  Commited To Music
//
//  Created by Simon Knapp on 4/9/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import UIKit

@IBDesignable
class CustomRoundShadowView: UIView {
    
    @IBInspectable var bgColor: UIColor = #colorLiteral(red: 0.2901960784, green: 0.5647058824, blue: 0.8862745098, alpha: 1) {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable var cornerRad: CGFloat = 10 {
        didSet {
            setupView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    override func prepareForInterfaceBuilder() {
        setupView()
    }
    
    func setupView() {
        self.clipsToBounds = false
        layer.cornerRadius = cornerRad
        layer.backgroundColor = bgColor.cgColor
        layer.shadowColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        layer.shadowOpacity = 0.7
        layer.shadowRadius = 5.0
        layer.shadowOffset = CGSize(width: 1, height: 2)
    }

}
