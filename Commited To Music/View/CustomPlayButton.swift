//
//  CustomPlayButton.swift
//  Commited To Music
//
//  Created by Simon Knapp on 4/5/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import UIKit
import pop

@objcMembers
class CustomPlayButton: UIButton {

    override func awakeFromNib() {
        
        setupButtonView()
    }
    
    func setupButtonView(){
        self.addTarget(self, action: #selector(shrinkBtn), for: .touchDown)
        self.addTarget(self, action: #selector(shrinkBtn), for: .touchDragEnter)
        self.addTarget(self, action: #selector(scaleAnimation), for: .touchDragExit)
        self.addTarget(self, action: #selector(scaleToOriginal), for: .touchUpInside)
        
    }

    func shrinkBtn(){
        let scale: CGFloat = 0.9
        let scaleAnim = POPBasicAnimation(propertyNamed: kPOPLayerScaleXY)
        scaleAnim?.toValue = NSValue(cgSize: CGSize(width: scale, height: scale))
        self.layer.pop_add(scaleAnim, forKey: "layerScaleSmallAnimation")
    }
    
    func scaleAnimation(){
        let scaleAnim = POPSpringAnimation(propertyNamed: kPOPLayerScaleXY)
        scaleAnim?.velocity = NSValue(cgSize: CGSize(width: 3.0, height: 3.0))
        scaleAnim?.toValue = NSValue(cgSize: CGSize(width: 1.0, height: 1.0))
        scaleAnim?.springBounciness = 18
        scaleAnim?.springSpeed = 6
        self.layer.pop_add(scaleAnim, forKey: "layerScaleSpringAnimation")
    }
    
    func scaleToOriginal(){
        let scaleAnim = POPBasicAnimation(propertyNamed: kPOPLayerScaleXY)
        scaleAnim?.toValue = NSValue(cgSize: CGSize(width: 1.0, height: 1.0))
        self.layer.pop_add(scaleAnim, forKey: "layerScaleDefaultAnimation")
    }
}
