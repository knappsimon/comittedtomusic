//
//  AlbumArtCollectionCell.swift
//  Commited To Music
//
//  Created by Simon Knapp on 4/5/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import UIKit

class AlbumArtCollectionCell: UICollectionViewCell {

    @IBOutlet weak var artworkImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
            styleCell()
    }

    func configCell(item: MediaItem, frame: CGSize) {
        let imageURL = item.artwork.imageURL(size: CGSize(width: frame.height, height: frame.height))
        
        if let image = ImageCacheMan().cachedImage(url: imageURL) {
            artworkImageView.image = image
        } else {
            self.artworkImageView.image = #imageLiteral(resourceName: "EmptyCellImg")
            ImageCacheMan().downloadImage(url: imageURL) { (img) in
                
                UIView.transition(with: self.artworkImageView, duration: 0.5, options: .transitionCrossDissolve, animations: {
                    self.artworkImageView.image = img
                    NotificationCenter.default.post(name: NSNotification.Name("artworkDownloaded"), object: nil)
                }, completion: nil)
                
            }
        }
    }
    
    func styleCell(){
        layer.cornerRadius = 10
        self.clipsToBounds = true
        layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowRadius = 5.0
    }
}
