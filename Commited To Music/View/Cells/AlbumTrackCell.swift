//
//  AlbumTrackCell.swift
//  Commited To Music
//
//  Created by Simon Knapp on 4/8/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import UIKit

class AlbumTrackCell: UITableViewCell {

    @IBOutlet weak var trackNumberLbl: UILabel!
    @IBOutlet weak var trackNameLbl: UILabel!
    @IBOutlet weak var artishNameLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(track: MediaItem, index: Int) {
        self.trackNumberLbl.text = String(index + 1)
        self.trackNameLbl.text = track.name
        self.artishNameLbl.text = track.artistName
    }

}
