//
//  SearchCellTableViewCell.swift
//  Commited To Music
//
//  Created by Simon Knapp on 3/30/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {
   
    @IBOutlet weak var artistLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var artworkImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        configImage()
    }
    
    func configImage() {
        artworkImg.layer.cornerRadius = 15
        artworkImg.clipsToBounds = true
    }

    func configCell(item: MediaItem) {
        self.nameLbl.text = item.name
        self.artistLbl.text = item.artistName
        
        let imageURL = item.artwork.imageURL(size: CGSize(width: artworkImg.frame.width, height: artworkImg.frame.height))
        
        if let image = ImageCacheMan().cachedImage(url: imageURL) {
            artworkImg.image = image
        } else {
            self.artworkImg.image = #imageLiteral(resourceName: "Record")
            ImageCacheMan().downloadImage(url: imageURL) { (img) in
                UIView.animate(withDuration: 0.5, animations: {
                    self.artworkImg.image = img
                })
            
            }
        }
    }

}
