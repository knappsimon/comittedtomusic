//
//  UnderlineTF.swift
//  Commited To Music
//
//  Created by Simon Knapp on 4/5/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import UIKit

class UnderlineTF: UITextField {
    @IBInspectable var lblCornerRad: CGFloat = 10 {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable var underline: Bool = true {
        didSet {
            setupView()
        }
    }
    
    override func prepareForInterfaceBuilder() {
        setupView()
    }
    
    override func awakeFromNib() {
       setupView()
    }
    
    var underlineView = UIView()
    
    func setupView(){
        self.clearsOnBeginEditing = true
        self.translatesAutoresizingMaskIntoConstraints = false
        underlineView.layer.cornerRadius = lblCornerRad
        if underline {
            underlineView = UIView.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            underlineView.backgroundColor = .white
            underlineView.translatesAutoresizingMaskIntoConstraints = false
            self.textColor = .white
            let placeHolder = self.placeholder ?? "Search the Apple Music Store"
            self.attributedPlaceholder = NSAttributedString(string: placeHolder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        }
        addSubview(underlineView)
        
        underlineView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        underlineView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        underlineView.rightAnchor.constraint(equalTo: rightAnchor).isActive = false
        underlineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
     }
    
    
}
