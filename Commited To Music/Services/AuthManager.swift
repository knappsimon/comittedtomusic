//
//  AuthManager.swift
//  Commited To Music
//
//  Created by Simon Knapp on 3/28/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import Foundation
import StoreKit
import MediaPlayer

@objcMembers
class AuthManager: NSObject {
    
    var userToken = ""
    let cloudServiceController = SKCloudServiceController()
    var cloudServiceCaps = SKCloudServiceCapability()
    var cloudServiceCountry = ""
    let musicManager: MusicManager
        
    static let cloudServiceUpdatedNotification = Notification.Name("cloudServiceUpdated")
    static let authorizationUpdatedNotification = Notification.Name("authorizationUpdated")

   
    init(appleMusicManager: MusicManager) {
        self.musicManager = appleMusicManager
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(requestFunctionality), name: .SKCloudServiceCapabilitiesDidChange, object: nil)
        
        if #available(iOS 11.0, *) {
            NotificationCenter.default.addObserver(self, selector: #selector(requestCountryCode), name: .SKStorefrontCountryCodeDidChange, object: nil)
        }
        
        if SKCloudServiceController.authorizationStatus() == .authorized {
            requestFunctionality()
        }
        
        if let token = UserDefaults.standard.string(forKey: USER_KEY) {
            userToken = token
             requestUserToken()
        } else {
            requestUserToken()
        }
        requestCountryCode()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .SKCloudServiceCapabilitiesDidChange, object: nil)
        
        if #available(iOS 11.0, *) {
             NotificationCenter.default.removeObserver(self, name: .SKStorefrontCountryCodeDidChange, object: nil)
        }
        
    }
}

// Request cloud services authorization
extension AuthManager {
   
    func requestAccess() {
        guard SKCloudServiceController.authorizationStatus() == .notDetermined else { return }
        
        SKCloudServiceController.requestAuthorization { [weak self] (status) in
            switch status {
            case .authorized:
                self?.requestFunctionality()
                self?.requestUserToken()
            case .denied:
                NotificationCenter.default.post(name: NSNotification.Name("accessDenied"), object: nil)
            case .restricted:
                break
            case .notDetermined:
                break
            }
        }
    }
    
    func requestLibraryAccess() {
        guard MPMediaLibrary.authorizationStatus() == .notDetermined else { return }
        
        MPMediaLibrary.requestAuthorization { (_) in
            NotificationCenter.default.post(name: AuthManager.cloudServiceUpdatedNotification, object: nil)
        }
    }
}

// SKCloudServices stuff

extension AuthManager {
    
     func requestCountryCode() {
        let completion: (String?, Error?) -> Void = { [weak self] (countryCode, error) in
            guard error == nil else {
                print("unable to work out country code...doh")
                return
            }
            guard let countryCode = countryCode else {
                print("Unexpected value from SKCloudServiceController for storefront country code.")
                return
            }
            self?.cloudServiceCountry = countryCode
            NotificationCenter.default.post(name: AuthManager.cloudServiceUpdatedNotification, object: nil)
        }
        
        if SKCloudServiceController.authorizationStatus() == .authorized {
            if #available(iOS 11.0, *) {
                cloudServiceController.requestStorefrontCountryCode(completionHandler: completion)
            } else {
//                MusicManager.performAppleMusicGetUserStorefront(userToken: userToken, completion: completion)
            }
        } else {
            determineRegionFromDeviceSettings(completion: completion)
        }
    }
    
     func requestFunctionality() {
        cloudServiceController.requestCapabilities { [weak self] (capabilities, error) in
            guard error == nil else {
                fatalError("An error occured requesting capabilities...\(error!.localizedDescription)")
            }
            self?.cloudServiceCaps = capabilities
            NotificationCenter.default.post(name: AuthManager.cloudServiceUpdatedNotification, object: nil)
        }
    }
    
    func determineRegionFromDeviceSettings(completion: @escaping (String?, Error?) -> Void) {
        let regionCode = Locale.current.regionCode ?? "us"
       
        MusicManager.instance.performStorefrontLockup(regionCode: regionCode, completion: completion)
    }
    
    func requestUserToken() {
        
        guard let devToken = MusicManager.fetchDeveloperToken() else { return }
        
        if SKCloudServiceController.authorizationStatus() == .authorized {
            let completionHandler: (String?, Error?) -> Void = { [weak self] (token, error) in
                
                guard error == nil else {
                    print("an error occured whilst getting the user token. Error: \(error?.localizedDescription)")
                    return
                }
                
                guard let token = token else {
                    print("Unexpected value from SKCloudServiceController for user token.")
                    return
                }
                
                self?.userToken = token
                UserDefaults.standard.set(token, forKey: USER_KEY)
                
                if self?.cloudServiceCountry == "" {
                    self?.requestCountryCode()
                }
                
                NotificationCenter.default.post(name: AuthManager.cloudServiceUpdatedNotification, object: nil)
            }
                if #available(iOS 11.0, *) {
                    cloudServiceController.requestUserToken(forDeveloperToken: devToken, completionHandler: completionHandler)
                } else {
                    cloudServiceController.requestPersonalizationToken(forClientToken: devToken, withCompletionHandler: completionHandler)
            }
        }
    }
}





