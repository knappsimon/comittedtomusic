//
//  MediaPlayerManager.swift
//  Commited To Music
//
//  Created by Simon Knapp on 3/30/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import UIKit
import MediaPlayer

@objcMembers
class MusicPlayerManager: NSObject {
    
    static let didUpdateState = NSNotification.Name("didUpdateState")
//
    fileprivate let nowPlayingInfoCenter = MPNowPlayingInfoCenter.default()
    
    var playbackQueue = [MediaItem]()
    var musicPlayerController: MPMusicPlayerController!
//    let appPlayer = MPMusicPlayerController.applicationQueuePlayer
   
    override init() {
        super.init()
        
        musicPlayerController = MPMusicPlayerController.applicationMusicPlayer
        musicPlayerController.beginGeneratingPlaybackNotifications()
        
        NotificationCenter.default.addObserver(self,
                                       selector: #selector(handleMusicPlayerControllerNowPlayingItemDidChange),
                                       name: .MPMusicPlayerControllerNowPlayingItemDidChange,
                                       object: musicPlayerController)
        
        NotificationCenter.default.addObserver(self,
                                       selector: #selector(handleMusicPlayerControllerPlaybackStateDidChange),
                                       name: .MPMusicPlayerControllerPlaybackStateDidChange,
                                       object: musicPlayerController)
        
        
    }
    
    deinit {
        musicPlayerController.endGeneratingPlaybackNotifications()
        
        NotificationCenter.default.removeObserver(self,
                                          name: .MPMusicPlayerControllerNowPlayingItemDidChange,
                                          object: musicPlayerController)
        NotificationCenter.default.removeObserver(self,
                                          name: .MPMusicPlayerControllerPlaybackStateDidChange,
                                          object: musicPlayerController)
    }
}

//Playback controls
extension MusicPlayerManager {
    
    func beginPlayback(itemCollection: MPMediaItemCollection) {
        musicPlayerController.setQueue(with: itemCollection)
        musicPlayerController.play()
    }
    
    func beginPlayback(itemID: [String]) {
        musicPlayerController.setQueue(with: itemID)
        
        musicPlayerController.play()
    }
    
    func togglePlayState() {
        if musicPlayerController.playbackState == .playing {
            musicPlayerController.pause()
        } else {
          musicPlayerController.play()
        }
    }
    
    
    func endPlayback() {
        musicPlayerController.stop()
//        musicPlayerController.perform({ (queue) in
//            queue.remove(self.musicPlayerController.nowPlayingItem!)
//        }) { (queue, error) in
//            guard error == nil else { return }
//            print(queue)
//        }
    }
}


// Handle remote events

//extension MusicPlayerManager {
//
//    func handleRemoteEvent(){
//
//        do {
//            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: .mixWithOthers)
//            print("Playback OK")
//            try AVAudioSession.sharedInstance().setActive(true)
//            print("Session is Active")
//        } catch {
//            print(error)
//        }
//    }
//
//    func nullCommand(){
//        print("...please dont skip")
//    }
//}

//Handle Notifications
extension MusicPlayerManager {
    
    func handleMusicPlayerControllerNowPlayingItemDidChange() {
        NotificationCenter.default.post(name: MusicPlayerManager.didUpdateState, object: nil)
    }
    
    func handleMusicPlayerControllerPlaybackStateDidChange() {
        NotificationCenter.default.post(name: MusicPlayerManager.didUpdateState, object: nil)
    }
    
}
