//
//  ImageCacheMan.swift
//  Commited To Music
//
//  Created by Simon Knapp on 4/3/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import UIKit

class ImageCacheMan {
    
    static let imageCache: NSCache<NSString, UIImage> = {
        
        let cache = NSCache<NSString, UIImage>()
        cache.name = "ImageCache"
        cache.countLimit = 20
        cache.totalCostLimit = 20 * 1024 * 1024
        
        return cache
    }()
    
    func cachedImage(url: URL) -> UIImage? {
        return ImageCacheMan.imageCache.object(forKey: url.absoluteString as NSString)
    }
    
    func downloadImage(url: URL, completion: @escaping ((UIImage?) -> Void)) {
        let task = URLSession.shared.dataTask(with: url){ (data, response, error) in
            guard error == nil, let urlResponse = response as? HTTPURLResponse, urlResponse.statusCode == 200, let data = data else {
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }
            
            if let image = UIImage(data:data) {
                ImageCacheMan.imageCache.setObject(image, forKey: url.absoluteString as NSString)
                
                DispatchQueue.main.async {
                    completion(image)
                }
            } else {
                DispatchQueue.main.async {
                    completion(#imageLiteral(resourceName: "EmptyCellImg"))
                }
            }
        }
        task.resume()
    }
}
