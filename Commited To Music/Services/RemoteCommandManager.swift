ou//
//  RemoteCommandManager.swift
//  Commited To Music
//
//  Created by Simon Knapp on 4/11/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import Foundation
import MediaPlayer


class RemoteCommandManager: NSObject {
    
    fileprivate let remoteCommandCenter: MPRemoteCommandCenter
    
    let musicPlayerManager: MusicPlayerManager
    let session: AVAudioSession
    init(musicPlayerManager: MusicPlayerManager) {
        self.musicPlayerManager = musicPlayerManager
        remoteCommandCenter = MPRemoteCommandCenter.shared()
        session = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSessionCategoryPlayback, with: AVAudioSessionCategoryOptions.allowAirPlay)
    
        } catch {
            print("unable to set up session")
        }
        for cat in session.availableCategories {
            print(cat)
        }
    }
    
    
    func disablePlaybackControls() {
        remoteCommandCenter.nextTrackCommand.isEnabled = true
        remoteCommandCenter.nextTrackCommand.addTarget(self, action: #selector(doNothing))
        remoteCommandCenter.previousTrackCommand.isEnabled = false
        remoteCommandCenter.previousTrackCommand.addTarget(self, action: #selector(doNothing))
        remoteCommandCenter.togglePlayPauseCommand.isEnabled = true
        remoteCommandCenter.togglePlayPauseCommand.addTarget(self, action: #selector(doNothing))
    }
    
    deinit {
        print("RemoteCommandCenter: deinit")
    }
    
    @objc func doNothing(){ }
    
   @objc func handlePlayPause(_ event: MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus {
    
        return .noActionableNowPlayingItem
    }
    
    
    
   @objc func handleNextTrackCommand(_ event: MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus {
        return .noActionableNowPlayingItem
    }
    
    @objc func handlePreviousTrackCommand(_ event: MPRemoteCommandEvent) -> MPRemoteCommandHandlerStatus {
        return .noActionableNowPlayingItem
    }
}
