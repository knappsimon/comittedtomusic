//
//  MusicManager.swift
//  Commited To Music
//
//  Created by Simon Knapp on 3/28/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import StoreKit
import UIKit

class MusicManager {
    
    typealias CatalogSearchCompletionHandler = (_ mediaItems: [[MediaItem]], _ error: Error?) -> Void
    typealias CatalogAlbumTracksCompletionHandler = (_ mediaItems: [MediaItem], _ error: Error?) -> Void
    typealias GetStoreFrontComptionHandler = (_ storeFront: String?, _ error: Error?) -> Void
    typealias GetRecentlyPlayedCompletionHandler = (_ mediaItems: [MediaItem], _ error: Error?) -> Void
    static var instance = MusicManager()
    
    lazy var urlSession: URLSession = {
        let urlSessionConfiguration = URLSessionConfiguration.default
        return URLSession(configuration: urlSessionConfiguration)
    }()
    
    func performStorefrontLockup(regionCode: String, completion: @escaping GetStoreFrontComptionHandler) {
        guard let developerToken = MusicManager.fetchDeveloperToken() else {
            fatalError("missing developer token")
        }
        
        let urlRequest = AppleMusicRequestFactory.createStorefrontsRequest(regionCode: regionCode, developerToken: developerToken)
        
        let task = urlSession.dataTask(with: urlRequest) { [weak self] (data, response, error) in
            guard error == nil, let urlResponse = response as? HTTPURLResponse, urlResponse.statusCode == 200 else {
                completion(nil, error)
                return
            }
            
            do {
                let id = try self?.processStoreFront(from: data!)
                completion(id, nil)
            } catch {
                fatalError("unable to determine storefront \(error.localizedDescription)")
            }
        }
        task.resume()
    }
    
    static func fetchDeveloperToken() -> String? {
        let developerAuthenticationToken: String? = developerToken
        return developerAuthenticationToken
    }
    
    func processStoreFront(from json: Data) throws -> String {
        guard let jsonDictionary = try JSONSerialization.jsonObject(with: json, options: []) as? [String:Any],
            let data = jsonDictionary[ResponseRootJSONKeys.data] as? [[String: Any]] else {
                throw SerializationError.missing(ResponseRootJSONKeys.data)
        }
        
        guard let identifier = data.first?[ResourceJSONKeys.identifier] as? String else {
            throw SerializationError.missing(ResourceJSONKeys.identifier)
        }
        return identifier
    }
    
    func performStoreFrontAlbumQuery(with albumID: String, countryCode: String, completion: @escaping CatalogAlbumTracksCompletionHandler) {
        guard let token = MusicManager.fetchDeveloperToken() else {
            fatalError()
        }
        
        let urlRequest = AppleMusicRequestFactory.createSelectedAlbumData(with: albumID, countryCode: countryCode, token: token)
        
        let task = urlSession.dataTask(with: urlRequest) { (data, response, error) in
            guard error == nil, let urlResponse = response as? HTTPURLResponse, urlResponse.statusCode == 200 else {
                let res = response as? HTTPURLResponse
                print(res?.statusCode)
                completion([], error)
                return
            }
            
            do {
                let mediaItems = try self.processAlbumTracksMediaItem(from: data!)
                completion(mediaItems, nil)
            } catch {
                fatalError("unable to serialize album json data")
            }
        }
        task.resume()
    }
    
    func performStoreFrontQuery(with term: String, countryCode: String, completion: @escaping CatalogSearchCompletionHandler) {
        guard let developerToken = MusicManager.fetchDeveloperToken() else {
            fatalError()
        }
        
        
        
        let urlRequest = AppleMusicRequestFactory.createSearchRequest(with: term, countryCode: countryCode, developerToken: developerToken)
        
        let task = urlSession.dataTask(with: urlRequest) { (data, response, error) in
            guard error == nil, let urlResponse = response as? HTTPURLResponse, urlResponse.statusCode == 200 else {
               let res = response as? HTTPURLResponse
                print(res?.statusCode)
                completion([], error)
                return
            }
            
            do {
                let mediaItems = try self.processMediaItemSections(from: data!)
                completion(mediaItems, nil)
                
            } catch {
                fatalError("an error occurred: \(error.localizedDescription)")
            }
        }
        task.resume()
    }
    
    
    func processAlbumTracksMediaItem(from json: Data) throws -> [MediaItem] {
        guard let jsonDictionary = try JSONSerialization.jsonObject(with: json, options: []) as? [String: Any],
            let results = jsonDictionary[ResponseRootJSONKeys.data] as? [[String:Any]] else {
                throw SerializationError.missing(ResponseRootJSONKeys.data)
        }
        
        var mediaItems = [MediaItem]()
        if let relationshipDict = results[0][ResourceTypeJSONKeys.relationships] as? [String: Any],
            let tracksDict = relationshipDict[ResourceTypeJSONKeys.tracks] as? [String: Any] {
            
            if let dataArray = tracksDict[ResponseRootJSONKeys.data] as? [[String: Any]] {
                let songMediaItem = try processMediaItems(from: dataArray)
                mediaItems = songMediaItem
            }
        } else {
            print("problem with results[0] or relationshipDict")
        }
        return mediaItems
    }
    
    func processMediaItemSections(from json: Data) throws -> [[MediaItem]] {
        guard let jsonDictionary = try JSONSerialization.jsonObject(with: json, options: []) as? [String: Any],
            let results = jsonDictionary[ResponseRootJSONKeys.results] as? [String: [String:Any]] else {
                throw SerializationError.missing(ResponseRootJSONKeys.results)
        }
        var mediaItems = [[MediaItem]]()
        if let songDict = results[ResourceTypeJSONKeys.songs] {
            
            if let dataArray = songDict[ResponseRootJSONKeys.data] as? [[String: Any]] {
                let songMediaItem = try processMediaItems(from: dataArray)
                mediaItems.append(songMediaItem)
            }
        }
        
        if let albumsDict = results[ResourceTypeJSONKeys.albums] {
            
            if let dataArray = albumsDict[ResponseRootJSONKeys.data] as? [[String:Any]] {
                let albumMediaItems = try processMediaItems(from: dataArray)
                mediaItems.append(albumMediaItems)
            }
        }
        return mediaItems
    }
    
    func processMediaItems(from json: [[String: Any]]) throws -> [MediaItem] {
        let songMediaItems = try json.map { try MediaItem(json: $0) }
        return songMediaItems
    }
}
