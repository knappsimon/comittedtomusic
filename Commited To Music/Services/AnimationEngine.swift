//
//  AnimationEngine.swift
//  Commited To Music
//
//  Created by Simon Knapp on 4/8/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import UIKit

class AnimationEngine: NSObject, UIViewControllerAnimatedTransitioning {
    
    let duration = 0.8
    var presenting = true
    var originFrame = CGRect.zero
    
    var dismissCompletion: (()->Void)?
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let toView = transitionContext.view(forKey: .to)!
        let albumView = presenting ? toView :
            transitionContext.view(forKey: .from)!
        
        let initialFrame = presenting ? originFrame : albumView.frame
        let finalFrame = presenting ? albumView.frame : CGRect(x: originFrame.midX, y: originFrame.midY, width: 1, height: 1)
        
        let xScaleFactor = presenting ? initialFrame.width / finalFrame.width : finalFrame.width / initialFrame.width
        
        let yScaleFactor = presenting ? initialFrame.height / finalFrame.height : finalFrame.height / initialFrame.height
        
        let scaleTransform = CGAffineTransform(scaleX: xScaleFactor,
                                               y: yScaleFactor)
        
        if presenting {
            albumView.transform = scaleTransform
            albumView.center = CGPoint( x: initialFrame.midX,  y: initialFrame.midY)
            albumView.clipsToBounds = true
        }
        
        containerView.addSubview(toView)
        containerView.bringSubview(toFront: albumView)
        
        UIView.animate(withDuration: duration, delay:0.0,
                       usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0,
                       animations: {
                        albumView.transform = self.presenting ? CGAffineTransform.identity : scaleTransform
                        albumView.center = CGPoint(x: finalFrame.midX, y: finalFrame.midY)
        },
                       completion: { _ in
                        if !self.presenting {
                            self.dismissCompletion?()
                        }
                        transitionContext.completeTransition(true)
        })
        
       
    }
    
    
}
