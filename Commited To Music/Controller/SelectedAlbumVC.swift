//
//  SelectedAlbumVC.swift
//  Commited To Music
//
//  Created by Simon Knapp on 4/8/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import UIKit
import AVFoundation

class SelectedAlbumVC: UIViewController {
    
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var albumNameLbl: UILabel!
    @IBOutlet weak var artistNameLbl: UILabel!
    @IBOutlet weak var albumBlurbLbl: UILabel!
    
    
    var album: MediaItem!
    var albumTracks: [MediaItem] = []
    var trackIDs: [String] = []
    
    var authorizationManager: AuthManager!
    let appleMusicManager = MusicManager()
    var musicPlayerManager: MusicPlayerManager!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        albumNameLbl.text = album.name
        artistNameLbl.text = album.artistName
        setupBgView()
        fetchAlbumTracks()
    }
    

    func fetchAlbumTracks() {
        let albumID = album.identifier
        print(authorizationManager.cloudServiceCountry)
        appleMusicManager.performStoreFrontAlbumQuery(with: albumID, countryCode: authorizationManager.cloudServiceCountry) { [weak self] (items, error) in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            self?.albumTracks = items
          
            for track in items {
                self?.trackIDs.append(track.identifier)
            }

            
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }
    
    func setupBgView() {
        let imgUrl = album.artwork.imageURL(size: CGSize(width: self.view.frame.height, height: self.view.frame.height))
        if let image = ImageCacheMan().cachedImage(url: imgUrl) {
            bgImageView.image = image
        } else {
//            self.bgImageView.image = #imageLiteral(resourceName: "EmptyCellImg")
            ImageCacheMan().downloadImage(url: imgUrl) { (img) in
               
                DispatchQueue.main.async {
                    
                    UIView.transition(with: self.bgImageView,
                                      duration: 0.5,
                                      options: .transitionCrossDissolve,
                                      animations: {
                                        self.bgImageView.image = img
                    }, completion: nil)
                }
            }
        }
    }
}


extension SelectedAlbumVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albumTracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumTrackCell", for: indexPath) as? AlbumTrackCell else {
            return UITableViewCell()
        }
        cell.configCell(track: albumTracks[indexPath.row], index: indexPath.row)
        return cell
    }
}

extension SelectedAlbumVC {
    
    @IBAction func backToSearchResults(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func togglePlayback(sender:UIButton) {
        self.musicPlayerManager.musicPlayerController.setQueue(with: self.trackIDs)
        let track = musicPlayerManager.musicPlayerController.nowPlayingItem
        if track == nil {
            musicPlayerManager.playbackQueue = albumTracks
            musicPlayerManager.musicPlayerController.play()
        } else if musicPlayerManager.musicPlayerController.playbackState == .playing {
            let track = track!
            if trackIDs.contains({ track.playbackStoreID }()) {
                // similar queue
                musicPlayerManager.musicPlayerController.pause()
            } else {
                // new album in queue....maybe
                musicPlayerManager.playbackQueue = albumTracks
                musicPlayerManager.musicPlayerController.play()
            }
        } else if musicPlayerManager.musicPlayerController.playbackState == .paused {
            musicPlayerManager.musicPlayerController.play()
        }
        //        musicPlayerManager.appPlayer.play()
    }
}
