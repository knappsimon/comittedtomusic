//
//  ViewController.swift
//  Commited To Music
//
//  Created by Simon Knapp on 3/28/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import UIKit
import StoreKit
import MediaPlayer

@objcMembers
class HomeVC: UIViewController {

    
    var authorizationManager: AuthManager!
    var userToken = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNotifications()
    }
    
    deinit {
        // Remove all notification observers.
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.removeObserver(self,
                                          name: AuthManager.cloudServiceUpdatedNotification,
                                          object: nil)
        notificationCenter.removeObserver(self,
                                          name: AuthManager.authorizationUpdatedNotification,
                                          object: nil)
        notificationCenter.removeObserver(self,
                                          name: .UIApplicationWillEnterForeground,
                                          object: nil)
    }

    @IBAction func requestAuthorization(_ sender: UIButton) {
        authorizationManager.requestAccess()
        
        authorizationManager.requestLibraryAccess()
        
        if SKCloudServiceController.authorizationStatus() == .authorized {
            MusicManager.instance.performStoreFrontQuery(with: "taylor swift", countryCode: "us") { (items, error) in
                guard error == nil else {
                    print(error?.localizedDescription)
                    return
                }
                print("number of items: \(items.count)")
                for item in items {
                    
                    for media in item {
                    
                        print(media.name)
                    }
                }
            }
        }
    }
}




extension HomeVC {
    
    func setupNotifications() {
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.addObserver(self,
                                       selector: #selector(handleAuthorizationManagerDidUpdateNotification),
                                       name: AuthManager.cloudServiceUpdatedNotification,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(handleAuthorizationManagerDidUpdateNotification),
                                       name: AuthManager.authorizationUpdatedNotification,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(handleAuthorizationManagerDidUpdateNotification),
                                       name: .UIApplicationWillEnterForeground,
                                       object: nil)
    }
    
    func handleAuthorizationManagerDidUpdateNotification() {
        DispatchQueue.main.async {
            if SKCloudServiceController.authorizationStatus() == .notDetermined || MPMediaLibrary.authorizationStatus() == .notDetermined {
                
            } else {
                print(self.authorizationManager.cloudServiceCaps)
                if self.authorizationManager.cloudServiceCaps.contains(.musicCatalogSubscriptionEligible) &&
                    !self.authorizationManager.cloudServiceCaps.contains(.musicCatalogPlayback) {
                
                }
                
            }
            
            DispatchQueue.main.async {
//                self.tableView.reloadData()
            }
        }
    }
}


