//
//  PlaybackVC.swift
//  Commited To Music
//
//  Created by Simon Knapp on 3/30/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import UIKit
import MediaPlayer
import Foundation

@objcMembers
class PlaybackVC: UIViewController {
    
    @IBOutlet weak var playbackTimeSlider: UISlider!
    @IBOutlet weak var remainingTimeLbl: UILabel!
    @IBOutlet weak var waveImg: UIImageView!
    @IBOutlet weak var bgImgView: UIImageView!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    
    //Playing Info
    @IBOutlet weak var artworkImgView: UIImageView!
    @IBOutlet weak var trackNameLbl: UILabel!
    @IBOutlet weak var artistAlbumLbl: UILabel!
    
    var playbackQueue = [MediaItem]()
    var musicPlayerManager: MusicPlayerManager!
    var appPlayer: MPMusicPlayerController!
    var waveframe: CGRect?
    
    var updater : CADisplayLink! = nil
    
    var lastPlayedTrackID: String {
        get {
            return UserDefaults.standard.value(forKey: LAST_TRACK_ID) as? String ?? ""
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: LAST_TRACK_ID)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRemoteTransportControls()
        appPlayer = musicPlayerManager.musicPlayerController
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleMusicPlayerManagerDidUpdateState),
                                               name: MusicPlayerManager.didUpdateState,
                                               object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupView()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: MusicPlayerManager.didUpdateState,
                                                  object: nil)
    }
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
}


// get started

extension PlaybackVC {
    
    func setupView() {
        
        playbackTimeSlider.isUserInteractionEnabled = false
        
        guard musicPlayerManager.playbackQueue.count > appPlayer.indexOfNowPlayingItem else { return }
        let track = musicPlayerManager.playbackQueue[appPlayer.indexOfNowPlayingItem]
        trackNameLbl.text = track.name
        artistAlbumLbl.text = "\(track.artistName)"
        //        TODO: add artwork functions
        let url = track.artwork.imageURL(size: artworkImgView.frame.size)
        if let img = ImageCacheMan().cachedImage(url: url) {
            artworkImgView.image = img
            
        } else {
            artworkImgView.image = #imageLiteral(resourceName: "EmptyCellImg")
            waveImg.isHidden = false
            visualEffectView.isHidden = true
            visualEffectView.alpha = 0.0
            downloadArtwork(url: url) { (success, image) in
                if success {
                    UIView.transition(with: self.artworkImgView,
                                      duration: 0.5,
                                      options: .transitionCrossDissolve,
                                      animations: {
                                        
                                        self.artworkImgView.image = image
                                        self.visualEffectView.isHidden = false
                                        self.visualEffectView.alpha = 1.0
                                        self.waveImg.alpha = 0.0
                                        self.bgImgView.isHidden = false
                                        self.bgImgView.image = image
                                        
                    }, completion: { _ in
                        self.waveImg.isHidden = true
                    })
                }
            }
        }
        setupSlider()
    }
    
}

// playback control buttons

extension PlaybackVC {
    
    fileprivate func checkQueuePlayLastPlayedItem() {
        if playbackQueue.isEmpty && lastPlayedTrackID != ""{
            appPlayer.setQueue(with: [lastPlayedTrackID])
            appPlayer.play()
            setupSlider()
        }
    }
    
    @IBAction func playPausePressed(sender: UIButton) {
        if appPlayer.playbackState == .paused {
            appPlayer.play()
            setupSlider()
        } else if appPlayer.playbackState == .stopped {
            checkQueuePlayLastPlayedItem()
        } else if appPlayer.playbackState == .playing {
            appPlayer.pause()
        }
    }
    
    func remainingTime(track: MPMediaItem) {
        let duration = track.playbackDuration
        let current = appPlayer.currentPlaybackTime
        let remainingTime: TimeInterval = (duration - current)
       
        self.remainingTimeLbl.text = remainingTime.timeStringFromInterval()
    }
    
    func trackAudio() {
        guard let track = musicPlayerManager.musicPlayerController.nowPlayingItem, appPlayer.playbackState == .playing else { return }
        
        remainingTime(track: track)
        let normalizedTime = appPlayer.currentPlaybackTime
        playbackTimeSlider.value = Float(normalizedTime)
    }
    
    @IBAction func cancelClicked(sender: AnyObject) {
        appPlayer.stop()
        updater.invalidate()
    }
    
    func handleMusicPlayerManagerDidUpdateState() {
        if appPlayer.playbackState == .playing {
            setupView()
            guard let newID = appPlayer.nowPlayingItem?.playbackStoreID else { return }
            lastPlayedTrackID = newID
        }
    }
    
    func setupSlider() {
        updater = CADisplayLink(target: self, selector: #selector(trackAudio))
        updater.preferredFramesPerSecond = 10
        updater.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
        playbackTimeSlider.minimumValue = 0
        playbackTimeSlider.maximumValue = Float(appPlayer.nowPlayingItem?.playbackDuration ?? 1.0)
    }
}

extension PlaybackVC {
    
    //    Download artwork functions
    
    func downloadArtwork(url:URL, completion: @escaping (Bool, UIImage?) -> ()) {
        ImageCacheMan().downloadImage(url: url) { (image) in
            if image == nil {
                completion(false, nil)
            } else {
                completion(true, image)
            }
        }
    }
    
    func setupRemoteTransportControls() {
        
        // Get the shared MPRemoteCommandCenter
        let commandCenter = MPRemoteCommandCenter.shared()
        // Add handler for Play Command
        commandCenter.playCommand.addTarget { [unowned self] event in
            print("play")
            return .success
        }
        
        // Add handler for Pause Command
        commandCenter.pauseCommand.addTarget { [unowned self] event in
            print("pause")
            return .success
        }
    }
}
