//
//  SearchCollectionCellVC.swift
//  Commited To Music
//
//  Created by Simon Knapp on 4/5/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import UIKit

@objcMembers
class SearchCollectionCellVC: UIViewController {
    
    @IBOutlet weak var albumArtCollectionView: UICollectionView!
    @IBOutlet weak var albumTitleLbl: UILabel!
    @IBOutlet weak var albumArtistLbl: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var searchTF: UnderlineTF!
    @IBOutlet weak var progressBar: UISlider!
    @IBOutlet weak var metaDataStack: UIStackView!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!

    
    var authorizationManager: AuthManager!
    let appleMusicManager = MusicManager()
    var musicPlayerManager: MusicPlayerManager!
    
    var albumArtFlow: AlbumCustomFlow!
    var mediaItems = [MediaItem]()
    var selectedAlbum: MediaItem?
    var selectedAlbumTracks = [MediaItem]()
    
    let animationEngine = AnimationEngine()
    
    let SEGUE_SELECTED_ALBUM = "showSelectedAlbumVC"
    
    private var pageWidth: CGFloat {
        return self.albumArtFlow.itemSize.width
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        searchTF.delegate = self
        
        albumArtCollectionView.delegate = self
        albumArtCollectionView.dataSource = self
        albumArtFlow = AlbumCustomFlow.configureLayout(collectionView: self.albumArtCollectionView, itemSize: CGSize(width: CELL_SIZE, height: CELL_SIZE), minimumLineSpacing: 0)
        registerNib()
        searchTermEntered(query: "avicii")
        searchTF.isHidden = true
        searchTF.alpha = 0
        scrollToBeginning()
        NotificationCenter.default.addObserver(self, selector: #selector(setBgImage), name: NSNotification.Name("artworkDownloaded"), object: nil)
        
        animationEngine.dismissCompletion = { }
        
        metaDataStack.isHidden = true
        visualEffectView.isHidden = true
        visualEffectView.alpha = 0
     }
    
    override func viewWillAppear(_ animated: Bool) {
        searchTF.isHidden = true
        searchTF.alpha = 0
        fixOffset()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setBgImage()
        displaySearchTF(canBecomeFirstResponder: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case SEGUE_SELECTED_ALBUM:
            let destination = segue.destination as! SelectedAlbumVC
            destination.album = selectedAlbum
            destination.authorizationManager = authorizationManager
            destination.musicPlayerManager = musicPlayerManager
        default:
            break
        }
    }
    

}


extension SearchCollectionCellVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func registerNib() {
        let nib = UINib(nibName: "AlbumArtCollectionCell", bundle: nil)
        albumArtCollectionView.register(nib, forCellWithReuseIdentifier: "AlbumArtCollectionCell")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if mediaItems.count > 0 {
            setupView()
            return mediaItems.count
        } else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlbumArtCollectionCell", for: indexPath) as? AlbumArtCollectionCell else { return UICollectionViewCell() }
        let item = mediaItems[indexPath.item]
        if indexPath.item == 0 {
            albumArtistLbl.text = item.artistName
            albumTitleLbl.text = item.name
        }
        
        cell.configCell(item: item, frame: CGSize(width: CELL_SIZE, height: CELL_SIZE))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CELL_SIZE, height: CELL_SIZE)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedAlbum = mediaItems[indexPath.row]
       guard let selectedAlbumVC = storyboard?.instantiateViewController(withIdentifier: "SelectedAlbumVC") as? SelectedAlbumVC else { return }
        selectedAlbumVC.album = mediaItems[indexPath.row]
        selectedAlbumVC.transitioningDelegate = self
        selectedAlbumVC.authorizationManager = authorizationManager
        selectedAlbumVC.musicPlayerManager = musicPlayerManager
        present(selectedAlbumVC, animated: true, completion: nil)
    }
    
    func scrollToBeginning(){
        let pageOffset = 0 * self.pageWidth - self.albumArtCollectionView.contentInset.left
        self.albumArtCollectionView.setContentOffset(CGPoint(x:pageOffset, y:0), animated: true)
        setBgImage()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        fixOffset()
    }
    
    func fixOffset(){
        let page = round(albumArtCollectionView.contentOffset.x / CGFloat(CELL_SIZE))
        setBgImage()
        if Int(page) < mediaItems.count - 1{
            let pageOffset = CGFloat(page) * self.pageWidth - self.albumArtCollectionView.contentInset.left
            self.albumArtCollectionView.setContentOffset(CGPoint(x:pageOffset, y:0), animated: true)
        } else {
            let lastPage = mediaItems.count - 1
            let pageOffset = CGFloat(lastPage) * self.pageWidth - self.albumArtCollectionView.contentInset.left
            self.albumArtCollectionView.setContentOffset(CGPoint(x:pageOffset, y:0), animated: true)
        }
    }
    
    func setBgImage(){
        let page = round(albumArtCollectionView.contentOffset.x / CGFloat(CELL_SIZE))
        if !mediaItems.isEmpty && Int(page) < mediaItems.count {
            let albums = mediaItems
            let album = albums[Int(page)]
            albumTitleLbl.text = album.name
            albumArtistLbl.text = album.artistName
            
            
            if let image = ImageCacheMan().cachedImage(url: album.artwork.imageURL(size: CGSize(width: CELL_SIZE, height: CELL_SIZE))) {
                UIView.transition(with: backgroundImageView,
                                  duration: 0.8,
                                  options: .transitionCrossDissolve,
                                  animations: {
                                    self.backgroundImageView.image = image
                                    self.visualEffectView.alpha = 1.0

                },
                                  completion: nil)
            }
        }
    }
    
    func searchTermEntered(query: String?) {
        self.searchAppleMusic(query: query ?? "", completion: { success in
            if success {
                DispatchQueue.main.async {
                    self.albumArtCollectionView.reloadData()
                    self.setBgImage()
                }
            }
        })
    }
    
    func searchAppleMusic(query: String, completion: @escaping (Bool) -> ()) {
        if authorizationManager.cloudServiceCountry == "" {
            authorizationManager.requestCountryCode()
        }
        
        appleMusicManager.performStoreFrontQuery(with: query, countryCode: authorizationManager.cloudServiceCountry) { (resultItems, error) in
            guard error == nil else {
                completion(false)
                return
            }
            if resultItems.count == 1 {
                self.mediaItems = resultItems[0]
                completion(true)
            } else {
                completion(false)
            }
        }
    }
}

extension SearchCollectionCellVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let query = textField.text {
            searchTermEntered(query: query)
            scrollToBeginning()
            view.endEditing(true)
        }
        return true
    }
}


extension SearchCollectionCellVC {
    
    func setupView() {
        metaDataStack.isHidden = false
        visualEffectView.isHidden = false
    }
    
    fileprivate func displaySearchTF(canBecomeFirstResponder: Bool) {
        UIView.animate(withDuration: 0.2, animations: {
            self.searchTF.isHidden = false
            self.searchTF.alpha = 1
            self.searchTF.placeholder = "Search The Apple Music Store"
            self.searchTF.attributedPlaceholder = NSAttributedString(string: self.searchTF.placeholder!, attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 1, alpha: 0)])
        }) { (finished) in
            if finished {
                UIView.animate(withDuration: 0.3, animations: {
                    self.searchTF.underlineView.frame.size.width = self.searchTF.frame.size.width
                    self.searchTF.underlineView.rightAnchor.constraint(equalTo: self.searchTF.rightAnchor).isActive = true
                    self.searchTF.attributedPlaceholder = NSAttributedString(string: self.searchTF.placeholder!, attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 1, alpha: 1)])
                })
                if canBecomeFirstResponder {
                    self.searchTF.becomeFirstResponder()
                }
            }
        }
    }
    
    @IBAction func searchIconPrd(sender: UIButton) {
        displaySearchTF(canBecomeFirstResponder: true)
    }
    
    @IBAction func playBtnPrd(){
        let page = round(albumArtCollectionView.contentOffset.x / CGFloat(CELL_SIZE))
        let selectedMedia = mediaItems[Int(page)]
//        musicPlayerManager.beginPlayback(itemID: selectedMedia.identifier)
        progressBar.maximumValue = Float(musicPlayerManager.musicPlayerController.nowPlayingItem!.playbackDuration)
        progressBar.setValue(0, animated: true)
        print(musicPlayerManager.musicPlayerController.nowPlayingItem!.playbackDuration)
        UIView.animate(withDuration: musicPlayerManager.musicPlayerController.nowPlayingItem!.playbackDuration) {
            self.progressBar.setValue(self.progressBar.maximumValue, animated: true)
        }
    }

}

extension SearchCollectionCellVC: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let cellIndex = round(albumArtCollectionView.contentOffset.x / CGFloat(CELL_SIZE))
        guard let cell = albumArtCollectionView.cellForItem(at: IndexPath(item: Int(cellIndex), section: 0)) else { return nil }
        
        let point = cell.superview!.convert(cell.frame, to: nil)
        
        animationEngine.originFrame = point
        animationEngine.presenting = true
        return animationEngine
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        animationEngine.presenting = false
        return animationEngine
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
    }
}
