//
//  Extensions-ViewController.swift
//  Commited To Music
//
//  Created by Simon Knapp on 4/5/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import UIKit


extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
