//
//  Constants.swift
//  Commited To Music
//
//  Created by Simon Knapp on 3/28/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import Foundation

//dev only
let developerToken = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjQzSzJDM0NaVzYifQ.eyJpc3MiOiJaMzQzR1BWOTQ3IiwiaWF0IjoxNTMwNjEzNzAxLCJleHAiOjE1MzA4NzI5MDF9.v_G33Vhedm_81ZGE0O5IcQIMTue9685A3nJC3os4A1TY4HhSGlacWapFO9u60qkHVxFRUNpznBX72hAtUNJAkA"
var remoteCommandManager: RemoteCommandManager!


// defaults
let USER_KEY = "storedUserKey"

enum SerializationError: Error {
    case missing(String)
}

/// Keys related to the `Response Root` JSON object in the Apple Music API.
struct ResponseRootJSONKeys {
    static let data = "data"
    
    static let results = "results"
}

/// Keys related to the `Resource` JSON object in the Apple Music API.
struct ResourceJSONKeys {
    static let identifier = "id"
    
    static let attributes = "attributes"
    
    static let type = "type"
}

/// The various keys needed for parsing a JSON response from the Apple Music Web Service.
struct ResourceTypeJSONKeys {
    static let songs = "songs"
    
    static let albums = "albums"
    
    static let relationships = "relationships"
    
    static let tracks = "tracks"
    
    static let musicVideos = "music-videos"
}

// Album Artwork Cell sizing

let CELL_SIZE = 300

let LAST_TRACK_ID = "lastTrackID"
