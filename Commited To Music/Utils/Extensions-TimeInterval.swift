//
//  Extensions-String.swift
//  Commited To Music
//
//  Created by Simon Knapp on 4/9/18.
//  Copyright © 2018 Simon Knapp. All rights reserved.
//

import Foundation

extension TimeInterval {
    
    func timeStringFromInterval() -> String {
        let interval = self
        let hours = (Int(interval) / 3600)
        let minutes = Int(interval / 60)
        let seconds = Int(interval) - (Int(interval / 60) * 60)
       
        var correctedSeconds:String {
            if seconds >= 10 {
                return String(describing: seconds)
            } else {
                return "0" + String(describing: seconds)
            }
        }
        
        let timeString = "-\(minutes):\(correctedSeconds)"
        return timeString
    }
}
